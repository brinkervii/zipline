local ZiplineAPI = require(script.Parent.ZiplineAPI)
local ZiplineWorker = require(script.Parent.ZiplineWorker)
local ZiplineGui = require(script.Parent.ZiplineGui)(plugin)

local ServerStorage = game:GetService("ServerStorage")

function killZipline()
	ZiplineWorker:Stop()
	ZiplineAPI:Leave()
end

local toolbar = plugin:CreateToolbar("Zipline")
local buttons = {}

buttons.start = toolbar:CreateButton("Start Zipline", "Start the zipline", "rbxassetid://2131069071")
buttons.stop = toolbar:CreateButton("Stop Zipline", "Stop the zipline", "rbxassetid://2131069135")
buttons.toggle = toolbar:CreateButton("Toggle Zipline GUI", "Toggles the Zipline syncing plugin", "rbxassetid://2133301260")

buttons.start.Click:Connect(function()
	ZiplineAPI:Announce("Roblox Studio")
	ZiplineWorker:Start()

	ZiplineGui:Show()
end)

buttons.stop.Click:Connect(function()
	killZipline()
end)

buttons.toggle.Click:Connect(function()
	ZiplineGui:Toggle()
end)

local ksName = "Zipline Killswitch"
local killswitch = ServerStorage:FindFirstChild(ksName)

if not killswitch then
	killswitch = Instance.new("BoolValue")
	killswitch.Name = ksName
	killswitch.Archivable = false
	killswitch.Parent = ServerStorage
else
	killswitch.Value = true
	wait(.1)
	killswitch.Value = false
end

killswitch.Changed:Connect(function()
	if killswitch.Value then
		killZipline()
	end
end)