package net.brinkervii.rockit.zipline;

import net.brinkervii.rockit.common.Message;
import net.brinkervii.rockit.common.RockitNode;
import net.brinkervii.rockit.common.ThreadedNode;
import net.brinkervii.rockit.common.WatchEventMessage;
import net.brinkervii.rockit.zipline.data.ClientHolder;

import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.util.HashMap;
import java.util.HashSet;
import java.util.concurrent.atomic.AtomicBoolean;
import java.util.concurrent.atomic.AtomicLong;

import static java.nio.file.StandardWatchEventKinds.ENTRY_CREATE;
import static java.nio.file.StandardWatchEventKinds.ENTRY_DELETE;

public class ChangeCollector extends RockitNode implements ThreadedNode {
	private final HashSet<Path> paths = new HashSet<>();
	private final AtomicBoolean running = new AtomicBoolean(false);
	private final AtomicLong lastChangeTime = new AtomicLong(System.currentTimeMillis());
	private final HashMap<Path, WatchEvent.Kind<?>> changeBuffer = new HashMap<>();
	private Thread flushThread = null;

	public ChangeCollector() {
		ClientHolder.getInstance().addClientAddedListener(client -> {
			var changeList = client.getChangeList();

			paths.forEach(path -> changeList.add(path, ENTRY_CREATE));
		});
	}

	@Override
	public void accept(Message message) {
		if (message instanceof WatchEventMessage) {
			final var watchEventMessage = (WatchEventMessage) message;
			onWatchEvent(watchEventMessage);
		}
	}

	private void onWatchEvent(WatchEventMessage watchEventMessage) {
		var event = watchEventMessage.getWatchEvent();
		var path = event.context();
		var kind = event.kind();

		if (ENTRY_CREATE.equals(kind)) {
			paths.add(path);
		} else if (ENTRY_DELETE.equals(kind)) {
			paths.remove(path);
		}

		synchronized (changeBuffer) {
			changeBuffer.put(path, kind);
		}

		lastChangeTime.set(System.currentTimeMillis());
	}

	private void flush() {
		var delta = System.currentTimeMillis() - lastChangeTime.get();
		if (delta < 1000L) {
			return;
		}

		synchronized (changeBuffer) {
			changeBuffer.forEach((key, value) -> ClientHolder.getInstance().addChange(key, value));
			changeBuffer.clear();
		}
	}

	@Override
	public void start() {
		running.set(true);

		this.flushThread = new Thread(() -> {
			while (running.get()) {
				flush();

				try {
					Thread.sleep(100L);
				} catch (InterruptedException ignored) {
					running.set(false);
				}
			}
		});

		this.flushThread.start();
	}

	@Override
	public void stop() {
		running.set(false);

		if (flushThread != null) {
			flushThread.interrupt();
			this.flushThread = null;
		}
	}
}
