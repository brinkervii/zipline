package net.brinkervii.rockit.zipline;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class SessionManager {
	private final Set<Session> activeSessions = new HashSet<>();

	public Session create() {
		final var session = new Session();
		activeSessions.add(session);
		return session;
	}

	public void remove(String sessionId) {
		final Set<Session> mutated = activeSessions.stream()
				.filter(s -> !s.getId().equals(sessionId))
				.collect(Collectors.toSet());

		this.activeSessions.clear();
		this.activeSessions.addAll(mutated);
	}
}
