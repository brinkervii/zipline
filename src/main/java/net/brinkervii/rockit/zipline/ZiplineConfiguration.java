package net.brinkervii.rockit.zipline;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.io.File;

@Data
@JsonSerialize
public class ZiplineConfiguration {
	private short port = 8080;
	private String watchDir = "./";

	public static File file() {
		return new File("zipline.json");
	}
}
