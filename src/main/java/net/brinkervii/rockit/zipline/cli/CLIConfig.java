package net.brinkervii.rockit.zipline.cli;

import lombok.extern.slf4j.Slf4j;
import net.brinkervii.rockit.zipline.State;
import net.brinkervii.rockit.zipline.ZiplineConfiguration;
import picocli.CommandLine;

import java.util.concurrent.Callable;

@Slf4j
@CommandLine.Command(name = "config", description = "Configure Zipline")
public class CLIConfig implements Callable<Void> {
	@CommandLine.Option(names = {"-l", "--list"})
	private boolean list = false;

	@CommandLine.Option(names = {"-p", "--port"})
	private short port = -1;

	@CommandLine.Option(names = {"-w", "--write"})
	private boolean write = false;

	@Override
	public Void call() throws Exception {
		final ZiplineConfiguration configuration = State.current().loadConfiguration();

		if (port >= 0) {
			configuration.setPort(port);
		}

		if (write) {
			if (State.current().writeConfiguration()) {
				log.info("Wrote configuration");
			} else {
				log.error("Something went wrong while writing the configuration");
			}
		}

		if (list) {
			System.out.println(configuration);
		}

		return null;
	}
}
