package net.brinkervii.rockit.zipline.web.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

@Data
@JsonSerialize
public class Announcement {
	private String name = "Undefined";
}
