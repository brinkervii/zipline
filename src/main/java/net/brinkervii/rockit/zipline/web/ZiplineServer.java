package net.brinkervii.rockit.zipline.web;

import lombok.Getter;
import lombok.Setter;
import net.brinkervii.rockit.common.FileWatcher;
import net.brinkervii.rockit.common.Message;
import net.brinkervii.rockit.common.RockitNode;
import net.brinkervii.rockit.common.ThreadedNode;
import net.brinkervii.rockit.zipline.ChangeCollector;
import net.brinkervii.rockit.zipline.State;
import net.brinkervii.rockit.zipline.web.config.ApplicationConfig;
import org.eclipse.jetty.server.Server;
import org.eclipse.jetty.servlet.ServletContextHandler;
import org.eclipse.jetty.servlet.ServletHolder;
import org.glassfish.jersey.servlet.ServletContainer;

public class ZiplineServer extends RockitNode implements ThreadedNode {
	private final Server server;
	private FileWatcher watcher = null;
	private ChangeCollector changeCollector = null;

	@Getter
	@Setter
	private boolean enableFileWatcher = false;

	public ZiplineServer(int port) {
		this.server = new Server(port);

		final var context = new ServletContextHandler(ServletContextHandler.NO_SESSIONS);
		context.setContextPath("/");
		server.setHandler(context);

		final var container = new ServletContainer(new ApplicationConfig());
		final var holder = new ServletHolder(container);
		holder.setInitOrder(0);

		context.addServlet(holder, "/*");
	}

	@Override
	public void start() throws Exception {
		this.changeCollector = new ChangeCollector();

		if (enableFileWatcher) {
			this.watcher = new FileWatcher(State.current().getCwd());
			watcher.addListener(changeCollector);
			watcher.start();
		}

		changeCollector.start();

		server.start();
		server.join();
	}

	@Override
	public void stop() throws Exception {
		if (watcher != null) {
			watcher.stop();
			this.watcher = null;
		}

		if (changeCollector != null) {
			changeCollector.stop();
			this.changeCollector = null;
		}

		server.stop();
	}

	@Override
	public void accept(Message message) {
		this.changeCollector.accept(message);
	}
}
