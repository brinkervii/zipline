package net.brinkervii.rockit.zipline.web.model;

import lombok.Data;

import java.util.ArrayList;

@Data
public class CreatePackageRequest {
	private ArrayList<String> changes = new ArrayList<>();
}
