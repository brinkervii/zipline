package net.brinkervii.rockit.zipline.web.controller;

import net.brinkervii.rockit.zipline.State;
import net.brinkervii.rockit.zipline.data.Client;
import net.brinkervii.rockit.zipline.data.ClientHolder;
import net.brinkervii.rockit.zipline.web.model.Announcement;
import net.brinkervii.rockit.zipline.web.model.AnnouncementResponse;
import net.brinkervii.rockit.zipline.web.model.StandardResponse;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;

@Path("announce")
public class AnnounceController {
	@Inject
	Client client;

	@Context
	ContainerRequestContext containerRequestContext;

	@POST
	@Produces(MediaType.APPLICATION_JSON)
	public AnnouncementResponse post(Announcement announcement) {
		final var session = State.current().getSessionManager().create();
		final var client = session.setAttribute("client",
				ClientHolder.getInstance().create(
						session.getId(),
						announcement.getName()
				)
		);

		return new AnnouncementResponse(client.getSessionId());
	}

	@DELETE
	@Produces(MediaType.APPLICATION_JSON)
	public StandardResponse delete() {
		ClientHolder.getInstance().deleteClient(client);
		State.current().getSessionManager().remove(client.getSessionId());
		return new StandardResponse();
	}
}
