package net.brinkervii.rockit.zipline.web.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import net.brinkervii.rockit.zipline.data.ChangeListEntry;

import java.util.HashMap;
import java.util.Map;

@Data
@JsonSerialize
public class ZiplinePackageTakeout {
	@JsonProperty
	private Map<String, String> content = new HashMap<>();
	@JsonProperty
	private int remainingChanges = 0;
	private int packageSize = 0;

	public int calculateSize() {
		int size = 0;
		for (String value : content.values()) {
			size += value.length();
		}

		return size;
	}

	public void put(ChangeListEntry change, String s) {
		content.put(change.getId(), s);
	}
}
