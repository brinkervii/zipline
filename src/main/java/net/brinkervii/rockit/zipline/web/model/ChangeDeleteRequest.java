package net.brinkervii.rockit.zipline.web.model;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;

import java.util.ArrayList;

@JsonSerialize
@Data
public class ChangeDeleteRequest {
	private ArrayList<String> changes = new ArrayList<>();
}
