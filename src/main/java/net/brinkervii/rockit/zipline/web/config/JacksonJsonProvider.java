package net.brinkervii.rockit.zipline.web.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.fasterxml.jackson.jaxrs.json.JacksonJaxbJsonProvider;
import net.brinkervii.rockit.zipline.KindSerializer;

import javax.annotation.Priority;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.ext.Provider;
import java.nio.file.WatchEvent;

@Provider
@Priority(0)
@Produces(MediaType.APPLICATION_JSON)
public class JacksonJsonProvider extends JacksonJaxbJsonProvider {
	private static ObjectMapper mapper = new ObjectMapper();

	static {
		final var module = new SimpleModule();
		module.addSerializer(WatchEvent.Kind.class, new KindSerializer());

		mapper.registerModule(module);
	}

	public JacksonJsonProvider() {
		super();
		setMapper(mapper);
	}
}
