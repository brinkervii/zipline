package net.brinkervii.rockit.zipline.data;

import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.util.*;

public class ChangeList {
	private final HashMap<Path, ChangeListEntry> changes = new HashMap<>();

	public void add(Path path, WatchEvent.Kind<?> kind) {
		final LinkedHashSet<Path> rubbish = new LinkedHashSet<>();

		synchronized (changes) {
			for (Map.Entry<Path, ChangeListEntry> entry : changes.entrySet()) {
				if (entry.getKey().toAbsolutePath().normalize().toString().equals(path.toAbsolutePath().normalize().toString())) {
					rubbish.add(entry.getKey());
				}
			}

			rubbish.forEach(changes::remove);

			changes.put(path, new ChangeListEntry(path, kind));
		}
	}

	public Collection<ChangeListEntry> getChanges() {
		return changes.values();
	}

	public void deleteChanges(ArrayList<String> oldChanges) {
		final LinkedHashSet<Path> rubbish = new LinkedHashSet<>();

		synchronized (this.changes) {
			for (Map.Entry<Path, ChangeListEntry> entry : this.changes.entrySet()) {
				for (String change : oldChanges) {
					if (change.equals(entry.getValue().getId())) {
						rubbish.add(entry.getKey());
					}
				}
			}

			rubbish.forEach(this.changes::remove);
		}
	}

	public int size() {
		synchronized (changes) {
			return changes.size();
		}
	}
}
