package net.brinkervii.rockit.zipline.data;

import lombok.extern.slf4j.Slf4j;
import org.glassfish.hk2.api.Factory;

import javax.ws.rs.core.Context;
import javax.ws.rs.core.HttpHeaders;

@Slf4j
public class ClientFactory implements Factory<Client> {
	private Client clientFromHeaders() {
		final var authorizationHeaders = headers.getRequestHeader("Authorization");
		if (authorizationHeaders == null) {
			return null;
		}

		final var authorizationHeader = authorizationHeaders.stream().findFirst();
		if (authorizationHeader.isEmpty()) {
			log.error("Authorization header is empty");
			return null;
		}

		final boolean isBearer = authorizationHeader.get().toLowerCase().startsWith("bearer ");
		if (!isBearer) {
			log.error("Authorization header is not a bearer");
			return null;
		}

		final String token = authorizationHeader.get().substring(7).trim();

		final var client = ClientHolder.getInstance().findClientById(token);

		if (client.isEmpty()) {
			log.error("Did not find a client for " + token);
			return null;
		}

		return client.get();
	}

	@Context
	HttpHeaders headers;

	@Override
	public Client provide() {
		return clientFromHeaders();
	}

	@Override
	public void dispose(Client client) {
		// Ignore disposal
	}
}
