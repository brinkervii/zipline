package net.brinkervii.rockit.zipline.data;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import lombok.Data;
import net.brinkervii.rockit.zipline.State;
import net.brinkervii.rockit.zipline.util.FileUtil;

import java.io.File;
import java.nio.file.Path;
import java.nio.file.WatchEvent;
import java.util.UUID;

@Data
@JsonSerialize
public class ChangeListEntry {
	private final static String PATH_SEPARATOR = (char) 0x5c + File.separator;

	private final String id = UUID.randomUUID().toString();
	private final WatchEvent.Kind<?> kind;
	private final String[] path;
	private final boolean isDirectory;
	private final boolean isFile;

	@JsonIgnore
	private final File file;

	public ChangeListEntry(Path path, WatchEvent.Kind<?> kind) {
		this.kind = kind;

		final File relativeFile = FileUtil.relativize(State.current().getCwd(), path.toFile());
		this.path = relativeFile.getPath().split(PATH_SEPARATOR);
		this.file = path.toFile();

		this.isDirectory = file.isDirectory();
		this.isFile = file.isFile();
	}
}
