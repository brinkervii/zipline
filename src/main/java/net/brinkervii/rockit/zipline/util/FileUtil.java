package net.brinkervii.rockit.zipline.util;

import net.brinkervii.rockit.zipline.ExceptionFunnel;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;

public class FileUtil {
	public static File relativize(File relativeTo, File file) {
		final Path relativeToPath = Paths.get(relativeTo.getAbsolutePath()).normalize();
		final Path filePath = Paths.get(file.getAbsolutePath()).normalize();
		final Path relativePath = relativeToPath.relativize(filePath);

		return relativePath.toFile();
	}

	public static String getHash(File file) throws IOException {
		MessageDigest digest = null;
		try {
			digest = MessageDigest.getInstance("SHA1");
		} catch (NoSuchAlgorithmException e) {
			ExceptionFunnel.accept(e);

			try {
				digest = MessageDigest.getInstance("MD5");
			} catch (NoSuchAlgorithmException e1) {
				ExceptionFunnel.accept(e1);
			}
		}

		if (digest == null) throw new IOException();

		byte[] buffer = new byte[1024];
		int count;
		try (FileInputStream inputStream = new FileInputStream(file)) {
			while ((count = inputStream.read(buffer)) != -1) {
				digest.update(buffer, 0, count);
			}
		}

		final byte[] digestedBytes = digest.digest();
		final StringBuilder stringBuilder = new StringBuilder();

		for (byte b : digestedBytes) {
			stringBuilder.append(Integer.toString((b & 0xff) + 0x100, 16).substring(1));
		}

		return stringBuilder.toString();
	}

	public static boolean equals(File left, File right) {
		final Path leftPath = left.getAbsoluteFile().toPath().normalize();
		final Path rightPath = right.getAbsoluteFile().toPath().normalize();

		return leftPath.toString().equals(rightPath.toString());
	}
}
