package net.brinkervii.rockit.zipline;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import java.io.IOException;
import java.nio.file.WatchEvent;

public class KindSerializer extends StdSerializer<WatchEvent.Kind> {
	protected KindSerializer(Class<WatchEvent.Kind> t) {
		super(t);
	}

	public KindSerializer() {
		this(null);
	}

	@Override
	public void serialize(WatchEvent.Kind value, JsonGenerator gen, SerializerProvider provider) throws IOException {
		gen.writeString(value.name());
	}
}
