package net.brinkervii.rockit.zipline;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import lombok.Data;
import lombok.extern.slf4j.Slf4j;

import java.io.File;
import java.io.IOException;
import java.nio.file.WatchEvent;

@Data
@Slf4j
public class State {
	private static State current = null;

	private final ObjectMapper jsonObjectMapper = new ObjectMapper();
	private final SessionManager sessionManager = new SessionManager();
	private File cwd;
	private ZiplineConfiguration configuration;

	public static State current() {
		if (current == null) current = new State();
		return current;
	}

	private State() {
		this.cwd = new File("./").getAbsoluteFile().toPath().normalize().toFile();

		final SimpleModule module = new SimpleModule();
		module.addSerializer(WatchEvent.Kind.class, new KindSerializer());
		jsonObjectMapper.registerModule(module);
	}

	public ZiplineConfiguration loadConfiguration() throws IOException {
		final File configurationFile = ZiplineConfiguration.file();
		if (configurationFile.exists()) {
			this.configuration = jsonObjectMapper.readValue(configurationFile, ZiplineConfiguration.class);
		} else {
			this.configuration = new ZiplineConfiguration();
		}

		return this.configuration;
	}

	public boolean writeConfiguration() throws IOException {
		if (configuration == null)
			return false;

		final File configurationFile = ZiplineConfiguration.file();
		jsonObjectMapper.writeValue(configurationFile, this.configuration);

		return true;
	}
}
